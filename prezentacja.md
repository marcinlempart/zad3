---
author: Marcin Lempart
title: Hello world w różnych językach programowania
subtitle: Prezentacja
date: 
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
---

## WPROWADZENIE

\begin{center}Prezentacja stanowi przedstawienie tekstu przywitania "Hello world" w różnych językach programowania.\end{center}

<p align=center> ![](pics/programowanie.jpg){ height=100% width=100%}</p> 

## Lista języków programowania do przedstawienia analizy

Lista języków programowania w jakich zostanie przedstawione hello world:
<!--- UWAGA WAŻNE pusta linia odstępu -->

1. Python
2. C
3. Java


<p></p>
<p></p>
\begin{alertblock}{Uwaga:}
Niniejsza prezentacja została stworzona w oparciu o prywatny ranking autora.
\end{alertblock}

## Lista najpopularniejszych języków programowania

| Ranking       | Język          | 
| ------------- |:-------------:| 
| 1    | Java | 
| 2      | Python   |  
| 3 | Javascript     |  
| 4 | PHP     | 
| 5 | C#     | 
| 6 | C     |
| 7 | R     |
| 8 | Objective-C     |
| 9 | Swift     |
| 10 | Matlab     |

## Hello world - Python

Python code:
```python
s = 2
print s
```
## Hello world - C

C code:
```c
#include<stdio.h>
int main(void)
{
    printf("Hello world\n");
    return 0;
}
```

## Hello world - Java

<b>Java code:</b>
```java
public class Hello {
  public static void main(String[] args){
    System.out.print("Hello World");
  }
}
```

## PODZIĘKOWANIA

\begin{center}Dziękuję za uwagę!\end{center}

